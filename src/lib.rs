pub mod scancodes;

use core::fmt;

use nom::{
    bytes::complete::{tag, take, take_while, take_while_m_n},
    combinator::{map_res, opt},
    multi::{count, separated_list0},
    IResult,
};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct HexDumpEntry {
    pub offset: usize,
    pub bytes: [u8; 16],
    pub ascii: Option<[char; 16]>,
}

impl HexDumpEntry {
    pub fn map_scancode(&self) -> [char; 16] {
        let vec: Vec<char> = self
            .bytes
            .into_iter()
            .map(|byte| {
                let scancode = scancodes::SCANCODES
                    .iter()
                    .find(|(code, _, _)| code == &byte);

                scancode.map_or('.', |(_, lower, _)| *lower)
            })
            .collect();

        vec.try_into().unwrap()
    }
}

impl fmt::Display for HexDumpEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self {
            offset,
            bytes,
            ascii,
        } = self;
        write!(f, "{offset:08x}:")?;
        for byte in bytes {
            write!(f, " {byte:02x}")?;
        }
        if let Some(ascii) = ascii {
            write!(f, "  ")?;
            for c in ascii {
                write!(f, "{c}")?;
            }
        }
        Ok(())
    }
}

fn from_hex(input: &str) -> Result<usize, std::num::ParseIntError> {
    usize::from_str_radix(input, 16)
}

fn from_hex_byte(input: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(input, 16)
}

const fn is_hex_digit(c: char) -> bool {
    c.is_ascii_hexdigit()
}

fn offset(input: &str) -> IResult<&str, usize> {
    map_res(take_while(is_hex_digit), from_hex)(input)
}

fn byte(input: &str) -> IResult<&str, u8> {
    let (input, _) = opt(tag(" "))(input)?;
    map_res(take_while_m_n(2, 2, is_hex_digit), from_hex_byte)(input)
}

fn bytes(input: &str) -> IResult<&str, [u8; 16]> {
    let (input, vec) = count(byte, 16)(input)?;
    let arr = vec.try_into().expect("there to be 16 bytes");
    Ok((input, arr))
}

fn ascii(input: &str) -> IResult<&str, [char; 16]> {
    let (input, _) = tag("  ")(input)?;
    let (input, chars) = take(16usize)(input)?;
    Ok((
        input,
        chars.chars().collect::<Vec<char>>().try_into().unwrap(),
    ))
}

fn parse_hexdump(input: &str) -> IResult<&str, HexDumpEntry> {
    let (input, offset) = offset(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, bytes) = bytes(input)?;
    let (input, ascii) = opt(ascii)(input)?;

    Ok((
        input,
        HexDumpEntry {
            offset,
            bytes,
            ascii,
        },
    ))
}

pub fn parse_hexdump_lines(input: &str) -> IResult<&str, Vec<HexDumpEntry>> {
    separated_list0(tag("\n"), parse_hexdump)(input)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn full() {
        let hexdump = r#"00000120: e401 2280 ea06 3004 fc10 6a8d b400 d1af  .."...0...j.....
00000130: 4414 2500 4909 0d00 4909 1330 5509 0208  D.%.I...I..0U...
00000140: 402e 5c98 0c00 6969 6d98 1212 0c0c 3998  @.\...iim.....9.
00000150: e401 5f5f 6062 0400 381e e808 0300 85f9  ..__`b..8.......
00000160: ff80 3000 6a5f 5600 d23a ff8f 0000 c057  ..0.j_V..:.....W
00000170: da71 8004 802c ca73 a40f 00c0 7d12 0080
00000180: 5205 0000 0000 0000 0000 6cf6 3300 7f16  R.........l.3..."#;

        let (input, entries) = parse_hexdump_lines(hexdump).unwrap();
        assert_eq!(input.len(), 0);
        assert_eq!(entries.len(), 7);

        let first = &entries[0];
        assert_eq!(first.offset, 0x120);
        assert_eq!(
            first.bytes,
            [
                0xe4, 0x01, 0x22, 0x80, 0xea, 0x06, 0x30, 0x04, 0xfc, 0x10, 0x6a, 0x8d, 0xb4, 0x00,
                0xd1, 0xaf
            ]
        );

        assert!(first.ascii.is_some());
        let ascii = first.ascii.unwrap();
        assert_eq!(
            ascii.iter().cloned().collect::<String>(),
            r#".."...0...j....."#
        );

        let sixth = &entries[5];
        assert!(sixth.ascii.is_none());
    }
}
