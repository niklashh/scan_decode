use std::io::prelude::*;
use std::io::stdin;

use scan_decode::parse_hexdump_lines;

fn main() {
    let lines = stdin()
        .lock()
        .lines()
        .collect::<Result<Vec<String>, _>>()
        .unwrap();
    let lines = lines.join("\n");

    let (_, entries) = parse_hexdump_lines(&lines).unwrap();

    for mut entry in entries {
        std::mem::take(&mut entry.ascii);
        let scancode_ascii: String = entry.map_scancode().into_iter().collect();
        println!("{entry} {scancode_ascii}"); // replace ascii with scan code
    }
}
